(define nil '())
(define nil? null?)

(define true #t)
(define true? (lambda [x] (equal? #t x)))

(define false #f)
(define false? (lambda [x] (equal? #f x)))

