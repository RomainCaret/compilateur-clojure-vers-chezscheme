;; start scheme session with (load "./src/minimal_compiler/runtime/core.scm")
(load "./src/minimal_compiler/runtime/consts.scm")
(load "./src/minimal_compiler/runtime/var.scm")
(load "./src/minimal_compiler/runtime/namespace.scm")
(load "./src/minimal_compiler/runtime/primitives.scm")

(define core (create-ns 'clojure.core))

;; compiler primitives
(define in-ns
    (lambda [symbol]
        (let ([namespace (create-ns symbol)])
            (set! *ns* namespace)
            namespace)))


;; numbers methods
(intern core '+ +)
(intern core '- -)
(intern core '* *)
(intern core '/ /)
(intern core 'mod mod)
(intern core '< <)
(intern core '<= <=)
(intern core '> >)
(intern core '>= >=)
(intern core '== =)

(intern core 'dec (lambda [x] (- x 1)))
(intern core 'inc (lambda [x] (+ x 1)))

(intern core 'zero? zero?)


;; numbes primitives
(define inc (lambda [n] (+ n 1)))
(define dec (lambda [n] (- n 1)))


;; general functions
(intern core 'apply apply)
(intern core 'print display)
(intern core 'println (lambda [x] (display x) (newline)))


;; exactly the same since '=' is type independent
(intern core '= equal?)

;; namespace methods
(intern core 'find-ns find-ns)
(intern core 'create-ns create-ns) 
(intern core 'remove-ns remove-ns) 
(intern core 'all-ns all-ns)
(intern core 'the-ns the-ns) 
(intern core 'ns-name ns-name) 
(intern core 'ns-map ns-map)
(intern core 'ns-unmap ns-unmap)
(intern core 'ns-resolve ns-resolve)
(intern core 'resolve resolve)
(intern core 'intern intern)


;; var methods
(intern core 'var-get var-get)
(intern core 'var-set var-set)
(intern core 'var? var?)
(intern core 'bound? bound?)

;; list methods
;; not exactly the same since clojure does not support pair, but only list
(intern core 'cons cons)


;; user namespace
(set! *ns* (create-ns 'user))