(ns minimal-compiler.clj.parser
  (:require
   [clojure.tools.analyzer :as ana :refer [-parse]]
   [clojure.tools.analyzer
    [utils :refer [-source-info]]]))


(def specials
  "Set of the special forms for scheme"
  (into ana/specials '#{ns scm* }))

(def ignored
  "Set of constructions we will evaluate but not compile"
  '#{defmacro})

(def not-inlined
  "Set of form that we don't want to be inlined"
  '#{+ - * / < <= > >= mod dec inc zero? =})

(defmulti parse
  "Extension to tools.analyzer/-parse for special forms"
  (fn [[op & rest] env] (if (ignored op) :ignored op)))




(defmethod parse :default
  [form env]
  (-parse form env))




(defmethod parse :ignored
  [form env]
  {:op :ignore
   :form form
   :env env})
  

;; version in tools.analyzer.js (commented)
;; it interprets the ns while creating an ast node

(defmethod parse 'ns
  [[_ name & args :as form] env]
  (when-not (symbol? name)
    (throw (ex-info (str "Namespaces must be named by a symbol, had: "
                         (.getName ^Class (class name)))
                    (merge {:form form}
                           (-source-info form env)))))
  (let [[docstring & args] (if (string? (first args))
                             args
                             (cons nil args))
        [metadata & args]  (if (map? (first args))
                             args
                             (cons {} args))
        name (vary-meta name merge metadata)
        ;; ns-opts (doto (desugar-ns-specs args form env)
        ;;           (validate-ns-specs form env)
        ;;           (populate-env name env))
        ]
    (merge
     {:op      :ns
      :env     env
      :form    form
      :name    name
;;    :depends (set (keys (:require ns-opts)))
      }
     (when docstring
       {:doc docstring})
     (when metadata
       {:meta metadata}))))


;; inspired from js* in tools.analyzer.js
;; (defmethod parse 'scm*
;;   [[_ scheme-expr & args :as form] env]
;;   (let [exprs (mapv (analyze-in-env (ctx env :ctx/expr)) args)]
;;     (merge
;;      {:op :scheme
;;       :env env
;;       :form form
;;       :expr scheme-expr}
;;      (when args
;;        {:children [:args]
;;         :args exprs}))))

