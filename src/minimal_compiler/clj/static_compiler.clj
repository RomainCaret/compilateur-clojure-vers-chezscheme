(ns minimal-compiler.clj.compiler
  (:require
   [clojure.string :as string]
   [minimal-compiler.clj.analyser :as min-ana]))




(defn pipe-ize
  [sym]
  (symbol (str "|" sym "|")))


(defmulti -emit-scheme
  (fn [{:keys [op]} _] op))

(defn emit-scheme
  ([ast] (-emit-scheme ast #{}))
  ([ast & opts] (-emit-scheme ast (into #{} opts))))


(defmethod -emit-scheme :const
  [{:keys [form]} _]
  form)

(defmethod -emit-scheme :ns
  [{:keys [name]} _]
  nil)

;; (defmethod -emit-scheme :var
;;   [{:keys [form]} _]
;;   `(~'get-from-current-namespace ~form))

(defmethod -emit-scheme :var
  [{:keys [var]} _]
  (let [meta-var (meta var)
        name (:name meta-var)
        ns-sym (ns-name (:ns meta-var))]
    (symbol (str ns-sym "/" name))))

;; (defmethod -emit-scheme :def
;;   [{:keys [name init]} opts]
;;   `(~'push-to-current-namespace ~name ~(if (nil? init) '(unboud-value) (-emit-scheme init opts))))

(defmethod -emit-scheme :def
  [{:keys [var init]} opts]
  (let [meta-var (meta var)
        name (:name meta-var)
        ns-sym (ns-name (:ns meta-var))]
  (list 'define (symbol (str ns-sym "/" name)) (if (nil? init) '(unbound) (-emit-scheme init opts)))))

(defmethod -emit-scheme :fn
  [{:keys [methods variadic?]} opts]
  (let [methods (sort-by #(+ (:fixed-arity %) (if (:variadic? %) 1 0)) methods)
        fixed-arity-methods (if variadic? (butlast methods) methods)
        fixed-arity-bindings (map #(vector (symbol (str "____f" (:fixed-arity %))) (-emit-scheme % opts)) fixed-arity-methods)
        variadic-method (when variadic? (last methods))
        variadic-binding (when variadic? (vector (symbol "____v") (-emit-scheme variadic-method opts)))
        bindings (if variadic? (cons variadic-binding fixed-arity-bindings) fixed-arity-bindings)
        error-arity '((else (arity-error ____len)))]

    (list 'let bindings
          (list 'lambda 'args (list 'let (list (vector '____len '(length args)))
                                    (concat (cons 'cond (map #(list (list '= '____len (:fixed-arity %))
                                                                    (list 'apply (symbol (str "____f" (:fixed-arity %))) 'args)) fixed-arity-methods))
                                            (if variadic? (cons (list (list '<= (:fixed-arity variadic-method) '____len) (list 'apply (symbol "____v") 'args)) error-arity) error-arity)))))))

(defmethod -emit-scheme :fn-method
  [{:keys [variadic? params body]} opts]
  (let [std-params (if variadic? (butlast params) params)
        v-param (when variadic? (last params))]
    `(~'lambda ~(if (and variadic? (= 1 (count params))) (pipe-ize (:name v-param)) (into (into [] (map #(pipe-ize (:name %)) std-params)) (when variadic? `(. ~(pipe-ize (:name v-param)))))) ~(-emit-scheme body opts))))

(defmethod -emit-scheme :do
  [{:keys [ret statements body?]} opts]
  (if (and body? (empty? statements))
    (-emit-scheme ret opts)
    `(~'begin
      ~@(map #(-emit-scheme % opts) statements)
      ~(-emit-scheme ret opts))))

; nil en scheme ? cas non géré
(defmethod -emit-scheme :if
  [{:keys [test then else]} opts]
  `(if ~(-emit-scheme test opts)
     ~(-emit-scheme then opts)
     ~@(when-not (nil? (:form else))
         [(-emit-scheme else opts)])))


(defmethod -emit-scheme :invoke
  [{:keys [fn args]} opts]
  (let [expr `(~(-emit-scheme fn opts)
               ~@(map #(-emit-scheme % opts) args))]
    expr))

(defmethod -emit-scheme :maybe-class
  [{:keys [form]} opts]
  form)

(defmethod -emit-scheme :with-meta
  [{:keys [expr]} opts]
  (-emit-scheme expr opts))


(defmethod -emit-scheme :local
  [{:keys [name]} _]
  (pipe-ize name))

(defmethod -emit-scheme :quote
  [{:keys [expr]} opts]
  (list 'quote (-emit-scheme expr opts)))

(defmethod -emit-scheme :ignore
  [{:keys [expr]} opts]
  'nil)


(defn compile-form [form]
  (let [current-ns *ns*
        compiled-expr (emit-scheme
                       (min-ana/analyze+eval form))]
    (set! *ns* current-ns)
    compiled-expr))

(defn compile-forms
  [& forms]
  (compile-form (cons 'do forms)))

(defmacro compile-prog
  [& forms]
  (list 'quote (apply compile-forms forms)))