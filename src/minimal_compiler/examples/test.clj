(ns user
  (:require [minimal-compiler.clj.compiler :as ncc]))


(ncc/compile-forms '(+ 1 2 3))
;; (begin ((var-get (ns-resolve 'clojure.core '+)) 1 2 3))


(ncc/compile-forms '(def add (fn ([] 0) ([& L] (apply + L)))))
;; (begin
;;  (intern
;;   *ns*
;;   'add
;;   (let
;;    ([____v
;;      (lambda |L__#0| ((var-get (ns-resolve 'clojure.core 'apply)) (var-get (ns-resolve 'clojure.core '+)) |L__#0|))]
;;     [____f0 (lambda [] 0)])
;;    (lambda
;;     args
;;     (let
;;      ([____len (length args)])
;;      (cond ((= ____len 0) (apply ____f0 args)) ((<= 0 ____len) (apply ____v args)) (else (arity-error ____len))))))))

(ncc/compile-forms '(add 1 2 3))
;; (begin ((var-get (ns-resolve 'user 'add)) 1 2 3))

(ncc/compile-forms '(def add (fn ([] 0) ([a] a) ([a & L] (+ a (apply add L))))))
;; (begin
;;  (intern
;;   *ns*
;;   'add
;;   (let
;;    ([____v
;;      (lambda
;;       [|a__#1| . |L__#0|]
;;       ((var-get (ns-resolve 'clojure.core '+))
;;        |a__#1|
;;        ((var-get (ns-resolve 'clojure.core 'apply)) (var-get (ns-resolve 'user 'add)) |L__#0|)))]
;;     [____f0 (lambda [] 0)]
;;     [____f1 (lambda [|a__#0|] |a__#0|)])
;;    (lambda
;;     args
;;     (let
;;      ([____len (length args)])
;;      (cond
;;       ((= ____len 0) (apply ____f0 args))
;;       ((= ____len 1) (apply ____f1 args))
;;       ((<= 1 ____len) (apply ____v args))
;;       (else (arity-error ____len))))))))


(ncc/compile-forms '(if (< 2 3) ((fn [x] x) 10) 34))
;; (begin
;;  (if
;;   ((var-get (ns-resolve 'clojure.core '<)) 2 3)
;;   ((let
;;     ([____f1 (lambda [|x__#0|] |x__#0|)])
;;     (lambda
;;      args
;;      (let ([____len (length args)]) (cond ((= ____len 1) (apply ____f1 args)) (else (arity-error ____len))))))
;;    10)
;;   34))

(ncc/compile-prog
 (ns ncc-examples.fact)

 (defn fact [n]
   (if (zero? n)
     1
     (* n (fact (dec n)))))

 (println (fact 10)))
;; (begin
;;  (in-ns 'ncc-examples.fact)
;;  (intern
;;   *ns*
;;   'fact
;;   (let
;;    ([____f1
;;      (lambda
;;       [|n__#0|]
;;       (if
;;        ((var-get (ns-resolve 'clojure.core 'zero?)) |n__#0|)
;;        1
;;        ((var-get (ns-resolve 'clojure.core '*))
;;         |n__#0|
;;         ((var-get (ns-resolve 'ncc-examples.fact 'fact)) ((var-get (ns-resolve 'clojure.core 'dec)) |n__#0|)))))])
;;    (lambda
;;     args
;;     (let ([____len (length args)]) (cond ((= ____len 1) (apply ____f1 args)) (else (arity-error ____len)))))))
;;  ((var-get (ns-resolve 'clojure.core 'println)) ((var-get (ns-resolve 'ncc-examples.fact 'fact)) 10)))


(ncc/compile-prog
 (ns ncc-examples.fact)

 (defmacro *' [expr1 expr2] (list '* expr1 expr2))

 (defn fact [n]
   (if (zero? n)
     1
     (*' n (fact (dec n)))))

 (println (fact 10)))
;; same as before except there is a new 'nil' that correspond to the ignored defmacro
;; (begin
;;  (in-ns 'ncc-examples.fact)
;;  nil                               ;; the new nil
;;  (intern
;;   *ns*
;;   'fact
;;   (let
;;    ([____f1
;;      (lambda
;;       [|n__#0|]
;;       (if
;;        ((var-get (ns-resolve 'clojure.core 'zero?)) |n__#0|)
;;        1
;;        ((var-get (ns-resolve 'clojure.core '*))
;;         |n__#0|
;;         ((var-get (ns-resolve 'ncc-examples.fact 'fact)) ((var-get (ns-resolve 'clojure.core 'dec)) |n__#0|)))))])
;;    (lambda
;;     args
;;     (let ([____len (length args)]) (cond ((= ____len 1) (apply ____f1 args)) (else (arity-error ____len)))))))
;;  ((var-get (ns-resolve 'clojure.core 'println)) ((var-get (ns-resolve 'ncc-examples.fact 'fact)) 10)))

