(ns minimal-compiler.test.test
  (:require [minimal-compiler.clj.compiler :as ncc]
            [scheme-generation.scheme :refer :all :rename {scheme-begin sdo
                                                           scheme-quoted-expr qexpr
                                                           scheme-expr sexpr
                                                           scheme-list slist
                                                           scheme-newline snewline
                                                           scheme-displayln sprintln
                                                           mscheme-can-expr svar
                                                           mscheme-let slet
                                                           mscheme-define sdef
                                                           mscheme-invoke sinvoke}]))

(defmacro run-test [title & expr]
  (sdo
   (snewline)
   (sprintln "TEST :" title)
   (sdef nb-tests 0)
   (sdef nb-succeed-tests 0)
   (apply sdo
          (map
           #(slet [quoted-expr (qexpr %)
                   generated-output (sexpr (ncc/compile-forms %))
                   expect-output %]
                  (sinvoke set! (svar nb-tests) (sinvoke + 1 (svar nb-tests)))
                  (sinvoke if
                           (sinvoke = (svar generated-output) (svar expect-output))
                           (sinvoke set! (svar nb-succeed-tests) (sinvoke + 1 (svar nb-succeed-tests)))
                           (sprintln "ERROR :" (svar quoted-expr) "outputs" (svar generated-output) "instead of" (svar expected-output) "...")))
           expr))
   (snewline)
   (sprintln (svar nb-succeed-tests) "success over" (svar nb-tests) "tests")))


(run-test "INT CONSTANTS" 42 52)

(run-test "RATIONNAL CONSTANTS" 1/2 3/4)

(run-test "FUNCTIONS"
          ((fn [x] x) 10)
          ((fn ([] 0) ([x] x) ([x & args] (apply + (cons x args)))) 10)
          ((fn ([] 0) ([x] x) ([x & args] (apply + (cons x args)))) 10 20 30))

;; Does not work because scheme does not accept define nested into let
;; (run-test "DEFN"
;;           (do (defn f ([] 0) ([x] (+ x (f))) ([x & y] (+ x (f y))))
;;               (f 10)))
