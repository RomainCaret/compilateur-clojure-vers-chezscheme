(define-record-type atom   
  (fields mutex (mutable item))
  (protocol 
   (lambda (new) 
           (lambda (obj) 
                   (new (make-mutex) obj)))))

(set! atom-item
  (let ([get-mutex (record-accessor (record-type-descriptor atom) 0)]
        [get-item (record-accessor (record-type-descriptor atom) 1)])
    (lambda (a)
            (with-mutex (get-mutex a) (get-item a)))))

(set! atom-item-set!
  (let ([get-mutex (record-accessor (record-type-descriptor atom) 0)]
        [set-item! (record-mutator (record-type-descriptor atom) 1)])
    (lambda (a value)
            (with-mutex (get-mutex a) (set-item! a value)))))

(define atom-swap!
  (let ([get-mutex (record-accessor (record-type-descriptor atom) 0)]
        [get-item (record-accessor (record-type-descriptor atom) 1)]
        [set-item! (record-mutator (record-type-descriptor atom) 1)])
    (lambda (a swap . args)
            (with-mutex (get-mutex a) (set-item! a (apply swap (cons (get-item a) args)))))))

(record-type-equal-procedure
 (record-type-descriptor atom)
 (lambda (a1 a2 eql?)
         (eql? (atom-item a1) (atom-item a2))))