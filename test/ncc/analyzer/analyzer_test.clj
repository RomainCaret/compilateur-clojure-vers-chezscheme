(ns ncc.analyzer.analyzer-test
  (:require
   [clojure.test :refer :all]
   ;; the JVM analyzer (clojure)
   [clojure.tools.analyzer.jvm :as jvm]
   ;; the clojurescript analyzer (clojure & clojurescript)
   [cljs.analyzer.api :as js]
   ;; our own analyzer (for now, fake piggybacking on the JVM)
   [ncc.analyzer.fake-analyzer :as a]))

(deftest test-analyze-simple
  (testing "constants"
    (is (= (a/analyze 42)
           {:op :const, :env {}, :type :number, :literal? true, :val 42, :form 42, :top-level true}))

    (is (= (select-keys (jvm/analyze 42) [:val :type :op])
           {:val 42, :type :number, :op :const}))

    (is (= (js/analyze {} 42)
           '{:op :const, :val 42, :env {}, :form 42, :tag number, :cljs.analyzer/analyzed true}))))





